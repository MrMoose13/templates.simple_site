// ---------------- МЕНЮ -----------------

(function () {
	const menu = document.querySelector('.burger');
	menu.addEventListener('click', () => {
		menu.classList.toggle('burgerActive');
		var nav = document.querySelector('.navigation');
		nav.classList.toggle('navigationActive');
	})
}());

// ---------------- ПЛАВНАЯ ПРОКРУТКА -----------------

const anchors = document.querySelectorAll('a[href*="#"]')

for (let anchor of anchors) {
  anchor.addEventListener('click', function (e) {
    e.preventDefault()
    
    const blockID = anchor.getAttribute('href').substr(1)
    
    document.getElementById(blockID).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    })
  })
}

// ---------------- ОТВЕТЫ НА ВОПРОСЫ -------------------

function more(el){                                    // more - имя функции, el - передаваемое значение
	el.closest('.answer').classList.toggle('active');    // .answer - ближайший родитель, toggle - добавляет/удаляет клас, active - добавляемый клас

}